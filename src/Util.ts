/**
 * Create a string which has the specified substring left aligned with gap filler.
 * @param subString  substring aligned to left.
 * @param totalLength  length of the created string.
 * @returns  aligned string
 */
export function createLeftAlignedString(subString: string, totalLength: number): string {
    const subStringLength = subString.length;
    if (subStringLength >= totalLength) {
        // Truncate.
        return subString.slice(0, totalLength);
    } else {
        // Add gap filler.
        return subString + ' '.repeat(totalLength - subStringLength);
    }
}
