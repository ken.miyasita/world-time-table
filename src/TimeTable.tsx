import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import FileCopyIcon from '@mui/icons-material/FileCopy';
import IconButton from '@mui/material/IconButton';
import {
  DataGrid,
  GridCellParams,
  GridColDef,
  GridRowData,
  GridRowsProp,
} from '@mui/x-data-grid';
import { DateTime } from 'luxon';
import React, { ReactElement } from 'react';
import { findTimeZone } from 'react-timezone-map-select';

const PLUS_COLUMN_WIDTH = 80;

/** 
 * Ordinary time zone column 
 */
const createHeaderColumnTimeZone = (timeZoneIndex: number, props: TimeTableProps): GridColDef => {
  const timeZoneName = props.timeZoneNames[timeZoneIndex];
  const timeZone = findTimeZone(timeZoneName) || { countryName: 'unknown', mainCities: ['unknown'] };

  const handleEdit = () => {
    props.onEditTimeZone(timeZoneIndex);
  };

  const handleDelete = () => {
    props.onDeleteTimeZone(timeZoneIndex);
  };

  const renderHeader = () => {
    // If there are two or more time zones, you can delete time zones.
    return (
      <div>
        {timeZone?.countryName + ' / ' + timeZone?.mainCities[0]}
        <IconButton aria-label="edit" onClick={handleEdit}>
          <EditIcon />
        </IconButton>
        {props.timeZoneNames.length >= 2 ? (
          <IconButton aria-label="delete" onClick={handleDelete}>
            <DeleteIcon />
          </IconButton>
        ) : null}
      </div>
    );
  }

  return {
    field: `${timeZoneIndex}`,
    headerName: timeZone.countryName + ' / ' + timeZone.mainCities[0],
    width: props.columnWidth,
    sortable: false,
    disableColumnMenu: true,
    renderHeader
  };

}

/**
 * The right most column (special column for "+" button and "copy" button)
 */
const createHeaderColumnPlus = (timeOriginZ: DateTime, props: TimeTableProps): GridColDef => {
  const handleAdd = () => {
    props.onAddTimeZone();
  };

  const handleCopy = (rowIndex: number): void => {
    const rowDateTime = timeOriginZ.plus({ hours: rowIndex });
    props.onCopyTimeZones(rowDateTime);
  }

  const renderHeader = () => {
    return (
      <div>
        <IconButton aria-label="add" onClick={handleAdd}>
          <AddCircleOutlineIcon />
        </IconButton>
      </div>
    );

  }

  const renderCell = (params: GridCellParams): ReactElement => {
    const rowIndex = params.id as number;
    return (
      <div>
        <IconButton aria-label="add" onClick={() => handleCopy(rowIndex)}>
          <FileCopyIcon />
        </IconButton>
      </div>
    );
  };

  return {
    field: 'plus',
    headerName: '',
    width: PLUS_COLUMN_WIDTH,
    sortable: false,
    disableColumnMenu: true,
    renderHeader,
    renderCell
  };

}

/**
 * Create the header row  (columns) 
 */
const createHeaderRow = (timeOriginZ: DateTime, props: TimeTableProps): GridColDef[] => {
  // Columns for time zones
  const columns = [];
  for (let i = 0; i < props.timeZoneNames.length; i = i + 1) {
    columns.push(createHeaderColumnTimeZone(i, props));
  }

  // add "+" column
  columns.push(createHeaderColumnPlus(timeOriginZ, props));

  return columns;
};

/**
 * Create rows displaying time of each city.
 */
const createBodyRows = (
  headerRow: GridColDef[],
  rowCount: number,
  timeOriginZ: DateTime,
  timeZoneNames: string[]
): GridRowsProp => {
  const rows = [];
  for (let rowIndex = 0; rowIndex < rowCount; rowIndex = rowIndex + 1) {
    const row: GridRowData = {};
    const timeZ = timeOriginZ.plus({ hours: rowIndex });

    // Construct a row.
    row.id = rowIndex;
    for (let columnIndex = 0; columnIndex < headerRow.length - 1; columnIndex = columnIndex + 1) {
      const timeZoneName = timeZoneNames[columnIndex];
      const localTime = timeZ.setZone(timeZoneName);
      row[`${columnIndex}`] = localTime.toLocaleString({
        weekday: 'short',
        month: 'short',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
      });
    }
    // Add the right most column  (dummy)
    row['plus'] = '';

    rows.push(row);
  }

  return rows;
};

interface TimeTableProps {
  /** Time zone names corresponding to columns.  e.g. "Asia/Tokyo" */
  timeZoneNames: string[];

  /** Function to be called when the Edit button in a time zone header is clicked. */
  onEditTimeZone: (timeZoneIndex: number) => void;

  /** Function to be called when the Delete button in a time zone header is clicked. */
  onDeleteTimeZone: (timeZoneIndex: number) => void;

  /** Function to be called when the Add button is clicked. */
  onAddTimeZone: () => void;

  /** Function to be called when the Copy button is clicked. */
  onCopyTimeZones: (dateTime: DateTime) => void;

  columnWidth: number;
  height: number;
  rowCount: number;
  rowHeight: number;
  width: number;

  /** The date and time user specified explicitly. */
  representativeDate: DateTime;
}

const TimeTable = (props: TimeTableProps): ReactElement => {
  // TimeTable show time rows from one day before the representative date, and allow users 
  // to select date flexibly.
  const timeOriginZ = props.representativeDate.minus({ days: 1 });

  const headerRow = createHeaderRow(timeOriginZ, props);
  const bodyRows = createBodyRows(headerRow, props.rowCount, timeOriginZ, props.timeZoneNames);

  return (
    <div style={{ height: '90vh', width: '100%' }}>
      <DataGrid rows={bodyRows} columns={headerRow} />
    </div>
  );
};

export default TimeTable;
