import AdapterDateFns from '@mui/lab/AdapterLuxon';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import React, { ReactElement } from 'react';

import TimeTableScreen from './TimeTableScreen';


function App(): ReactElement {
  return (
    <div>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <TimeTableScreen />
      </LocalizationProvider>
    </div>
  );
}

export default App;
